import java.util.Random;

public class Calculator {
	
	public static int addNums(int inputOne, int inputTwo) {
		int sum = inputOne + inputTwo;
		return sum;
	}
	
	public static double squareRoot(int inputOne) {
		double sqrt = Math.sqrt(inputOne);
		return sqrt;
	}
	
	public static int randomNum() {
		Random random = new Random();
		return random.nextInt();
	}
	
	public static double divideNums(double inputOne, double inputTwo) {
		double dividedNum = inputOne / inputTwo;
		return dividedNum;
	}
	
}