import java.util.Scanner;

public class Driver {
	
	public static void main (String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.println("Hello!");
		
		System.out.println("Please input a whole number:");
		int addOne = reader.nextInt();
		System.out.println("Please input another whole number:");
		int addTwo = reader.nextInt();
		int sum = Calculator.addNums(addOne, addTwo);
		System.out.println("Your sum is " + sum);
		
		System.out.println("Please input another whole number:");
		int sqrtOne = reader.nextInt();
		double sqrt = Calculator.squareRoot(sqrtOne);
		System.out.println("The square root of " + sqrtOne + " is: " + sqrt);
		
		int randomNum = Calculator.randomNum();
		System.out.println("A random number is: " + randomNum);
		
		System.out.println("Please input any any number:");
		double divOne = reader.nextDouble();
		double divTwo = 0;
		while (divTwo == 0) {
			System.out.println("Please input any number but 0:");
			divTwo = reader.nextDouble();
		}
		double dividedNum = Calculator.divideNums(divOne, divTwo);
		System.out.println(divOne + " divided by " + divTwo + " is: "+ dividedNum);
	}
	
}